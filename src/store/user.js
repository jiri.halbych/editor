import { defineStore } from "pinia";
import axios from 'axios'
import router from '../router/index'


export const useUserStore = defineStore("user", {
  state: () => ({
    auth_token: null,
    refresh_token: null,
    expiration: null
  }),

  

  actions: {
    async signIn(email, password) {
      let res = await axios.post("https://cognoro.xyz/users/sign_in", {"email": email, "password": password }, {headers: {
          "Content-Type": "application/json",
        },
      });
      this.auth_token = res.headers["access-token"];
      this.refresh_token = res.headers["refresh-token"];
      this.expiration = res.headers["expire-at"];
    },
    $reset() {
      this.auth_token = null;
      this.refresh_token = null;
      this.expiration = null;
    },
    async refresh() {
      try{
        let res = await axios.post("https://cognoro.xyz/users/tokens", {},{headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + this.auth_token,
            "Refresh-Token": this.refresh_token
          }},
        );
        this.auth_token = res.headers["access-token"];
        this.refresh_token = res.headers["refresh-token"];
        this.expiration = res.headers["expire-at"];
      } catch(error){
        this.auth_token = null;
        this.refresh_token = null;
        this.expiration = null;
        localStorage.removeItem("user");
        router.push('/login');
      }},
  },
  persist: true,
});