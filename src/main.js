import './assets/main.css'

import { createApp } from 'vue'
//import Vueform from '@vueform/vueform'
//import vueformConfig from '../vueform.config'
import App from './App.vue'
import router from './router'
import { createPinia } from "pinia";
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

const pinia = createPinia();
pinia.use(piniaPluginPersistedstate)

const app = createApp(App)

app.use(router)
//app.use(Vueform, vueformConfig)
app.use(pinia)
app.mount('#app')
